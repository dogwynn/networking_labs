# Networking for System Administrators

## Requirements

- Python3.7+
- Docker:
    - Docker for Windows
    - Docker for Mac

## Networking Labs

Each of the labs corresponds to the chapters of N4SA.

### Chapter 1: Network Layers

### Chapter 2: Ethernet

### Chapter 3: IPv4

### Chapter 4: IPv6

### Chapter 5: TCP/IP

### Chapter 6: Viewing Network Connections

### Chapter 7: Network Testing Basics

### Chapter 8: The Domain Name System (DNS)

### Chapter 9: Packet Sniffing

### Chapter 10: Creating Traffic

### Chapter 11: Server Packet Filtering

### Chapter 12: Tracing Problems


