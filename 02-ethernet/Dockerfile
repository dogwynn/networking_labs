FROM       python:3.7-slim
MAINTAINER "David O'Gwynn <dogwynn@lowlandresearch.com>"

RUN apt-get update

RUN apt-get install -y openssh-server supervisor bash-completion net-tools iproute2 iputils-ping nmap tcpdump tshark fping bind9-host libczmq-dev

RUN mkdir -p /var/run/sshd
RUN mkdir -p /var/log/supervisor

RUN echo 'root:root' |chpasswd

RUN sed -ri 's/^#?PermitRootLogin\s+.*/PermitRootLogin yes/' \
    /etc/ssh/sshd_config
RUN sed -ri 's/UsePAM yes/#UsePAM yes/g' /etc/ssh/sshd_config

RUN mkdir /root/.ssh

RUN apt-get clean && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

ENV PYTHON_PATH /netlab-install/python
RUN mkdir -p $PYTHON_PATH

WORKDIR $PYTHON_PATH

COPY requirements.txt .
RUN pip install -r requirements.txt

ENV LAB_PATH /lab
RUN mkdir -p $LAB_PATH

WORKDIR $LAB_PATH

COPY lab .

COPY supervisor.conf /etc/supervisor/conf.d/supervisor.conf

CMD  ["/usr/bin/supervisord"]
